export const MAIN_COLOR = '#5f70b2';
export const MAIN_BACKGROUND_COLOR = '#ffffff';
export const MAIN_BACKGROUND_COLOR_DARK = '#222121';

export const TEXT_COLOR_DARK = '#545660';
export const TEXT_COLOR_LIGHT = '#f2f2f2';

export const SECONDARY_TEXT = '#8c8c8c';

export const INPUT_BORDER_COLOR = 'gray';

export const HOROSCOPE_ITEM_BACKGROUND = '#e3e3e3';


