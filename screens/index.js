import { Home } from './home';
import { ProbabilityResponse } from './probability-response';
import { PercentPage } from './percent';
export {
  Home,
  ProbabilityResponse,
  PercentPage
}