import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';
import * as styleVariables from '../constants/style-variables';

export const PercentPage = () => {
  let percent = 70;
  const { currentMoonPhase } = useSelector(state => state);
  switch (currentMoonPhase) {
    case 'Новолуние': percent = 2;  break;  
    case 'Молодая луна': percent = 15; break;
    case 'Первая четверть': percent = 25; break;
    case 'Растущая луна': percent = 50;break;
    case 'Полнолуние': percent = 75; break;
    case 'Убывающая луна': percent = 80; break;
    case 'Старая луна': percent = 99; break;
    default: percent = 99; break;
  }

  return (
    <Container>
      <Title>{`Сейчас ${currentMoonPhase}, а это значит, что шанс сдать лабу = ${percent}%`}</Title>
    </Container>
  );
  
}

const Container = styled.ScrollView`
  flex: 1;
  padding: 5px;
`;


const Title = styled.Text`
  text-align: center;
  font-size: 26px;
  font-weight: bold;
  color: ${styleVariables.MAIN_COLOR};
`;