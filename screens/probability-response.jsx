import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from 'styled-components/native';
import * as styleVariables from '../constants/style-variables';
import { getCurrentMoonPhase } from './../redux/reducer';
import { FontAwesome } from '@expo/vector-icons'; 

export const ProbabilityResponse = ({navigation}) => {
  const dispatch = useDispatch();
  const { groupNumber, currentWeek, weekStart, weekEnd, timetable } = useSelector(state => state);
  
  useEffect(() => {
    navigation.setOptions({ title: `Группа ${groupNumber}` });
  }, [groupNumber])


  useEffect(() => {
    dispatch(getCurrentMoonPhase())
  }, [])

  return (
    <>
      <PercentContainer onPress={() => navigation.push('PercentPage')}>
        <FontAwesome name="percent" size={36} color={styleVariables.TEXT_COLOR_LIGHT} />
      </PercentContainer>

      <Container>
        <Title>Сейчас - {currentWeek} неделя</Title>
        <TitleSecondary>{ `${weekStart} - ${weekEnd}`}</TitleSecondary>
        {
          timetable.map(({id, day, lessons}) => {
            return (
              <DayContainer key={id}>
                <Day>{day}</Day>
                {
                  lessons.map(l => {
                    return (
                      <Lesson key={l.id}>
                        <Top>
                          <TopLeft>
                            <Name>{`${l.lesson}(${l.lessonType})`}</Name>
                            <Teacher>{l.teacher}</Teacher>
                          </TopLeft>
                          <Auditory>{l.auditory}</Auditory>
                        </Top>
                        <Bottom>
                          <LessonTime>{l.lessonTime}</LessonTime>
                          <Weeks>Недели - 
                            {
                              l.weeks.map(w => ` ${w}`)
                            }
                          </Weeks>
                        </Bottom>
                      </Lesson>
                    )
                  })
                }
              </DayContainer>
            ) 
          })
        }
      </Container>
    </>
    
  )
}

const PercentContainer = styled.TouchableOpacity`
  z-index: 999;
  position: absolute;
  top: -46px;
  right: 5px;
  width: 36px;
  height: 36px;
`;
const Container = styled.ScrollView`
  flex: 1;
  padding: 5px;
  background-color: ${styleVariables.MAIN_BACKGROUND_COLOR};
`;


const Title = styled.Text`
  text-align: center;
  font-size: 26px;
  font-weight: bold;
  color: ${styleVariables.MAIN_COLOR};
`;

const TitleSecondary = styled.Text`
  text-align: center;
  font-size: 18px;
  color: ${styleVariables.SECONDARY_TEXT};
`;

const DayContainer = styled.View`
  margin-top: 10px;
`;

const Day = styled.Text`
  text-align: center;
  font-size: 22px;
  color: ${styleVariables.TEXT_COLOR_LIGHT};
  background-color: ${styleVariables.MAIN_COLOR};
  margin-bottom: 5px;
  padding: 5px 0;
`;

const Lesson = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${styleVariables.TEXT_COLOR_DARK};
  flex: 1;
`;

const Top = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Bottom = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const TopLeft = styled.View`
  flex-direction: row;
`;

const Name = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: ${styleVariables.TEXT_COLOR_DARK};
  margin-right: 10px;
`;

const Teacher = styled.Text`
  font-size: 18px;
  color: ${styleVariables.TEXT_COLOR_DARK};
`;

const Auditory = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: ${styleVariables.TEXT_COLOR_DARK};
`;

const LessonTime = styled.Text`
  font-size: 18px;
  color: ${styleVariables.SECONDARY_TEXT};
`;

const Weeks = styled.Text`
  font-size: 18px;
  color: ${styleVariables.SECONDARY_TEXT};
`;