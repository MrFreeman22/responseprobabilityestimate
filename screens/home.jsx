import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';
import * as styleVariables from './../constants/style-variables';
import { AntDesign } from '@expo/vector-icons';
import { Alert } from "react-native"; 
import { Keyboard} from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { setGroupNumber, setIsActiveBtn } from './../redux/actions';
import { getTimetable } from './../redux/reducer';

export const Home = ({ navigation }) => {
  const dispatch =  useDispatch();
  const { groupNumber, isActiveBtn } = useSelector(state => state);

  //let activeContinueBtn = groupNumber.length === 6;

  useEffect(() => {
    if(groupNumber.length === 6) dispatch(getTimetable(groupNumber));
    if(groupNumber.length < 6) dispatch(setIsActiveBtn(true));
  }, [groupNumber])

  const About = () => {
    Alert.alert(
      'О Создателе',
      'Создал - Бурак А.Н., гр. 881061',
      [{ text: "Тап"}],
      { cancelable: false }
    );
  }

  return (
    <Fragment>
      <Info onPress={About}>
        <AntDesign name="infocirlceo" size={32} color={styleVariables.TEXT_COLOR_LIGHT} />
      </Info>
      <Container>
        <Title>Введите номер группы</Title>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <Input
            value={groupNumber}
            onChangeText={groupNumber => dispatch(setGroupNumber(groupNumber))}
            placeholder='6 цифр'
            keyboardType='numeric'
            maxLength={6}
          />
        </TouchableWithoutFeedback> 
        {
          isActiveBtn ? <ContinueBtn><ContinueBtnText>Корректно заполните поле</ContinueBtnText></ContinueBtn>  
          :<ContinueBtnActive onPress={() => navigation.push('ProbabilityResponse')}><ContinueBtnText>Узнать расписание</ContinueBtnText></ContinueBtnActive>                   
        }
      </Container>
    </Fragment>
    
  )
  
}

const Container = styled.ScrollView`
  flex: 1;
  padding: 15px 5px 0 5px;
`;

const Info = styled.TouchableOpacity`
  position: absolute;
  z-index: 99;
  top: -44px;
  right: 14px;
`;

const Title = styled.Text`
  text-align: center;
  font-size: 30px;
  color: ${styleVariables.MAIN_COLOR};
`;

const Input = styled.TextInput`
  width: 100%;
  border: none;
  border-bottom-width: 2px;
  border-bottom-color: ${styleVariables.INPUT_BORDER_COLOR};
  margin-top: 15px;
  padding: 10px 5px;
  color: ${styleVariables.TEXT_COLOR_DARK};
  font-size: 18px;
`;


const ContinueBtn = styled.View`
  justify-content: center;
  width: 100%;
  height: 60px;
  margin-top: 40px;
  border-radius: 5px;
  background-color: ${styleVariables.TEXT_COLOR_DARK};
`;

const ContinueBtnActive = styled.TouchableOpacity`
  justify-content: center;
  width: 100%;
  height: 60px;
  margin-top: 40px;
  border-radius: 5px;
  background-color: ${styleVariables.MAIN_COLOR};
`;

const ContinueBtnText = styled.Text`
  text-align: center;
  font-size: 20px;
  color: ${styleVariables.TEXT_COLOR_LIGHT};
`;
