import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Home, ProbabilityResponse, PercentPage } from './screens';
import { Provider } from 'react-redux';
import store from './redux/store';
import * as styleVariables from './constants/style-variables';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer >
        <Stack.Navigator  initialRouteName='Home'
                          screenOptions={{
                            headerStyle: {
                              backgroundColor: styleVariables.MAIN_COLOR,
                            },
                            headerTintColor: styleVariables.TEXT_COLOR_LIGHT,
                            headerTitleStyle: {
                              fontWeight: 'bold'
                            },
                          }}>

          <Stack.Screen name='Home' options={{ title: 'Ввод номера группы' }}>
            {props => <Home {...props} />}
          </Stack.Screen>

          <Stack.Screen name='ProbabilityResponse' options={{ title: 'Расписание' }}>
            {props => <ProbabilityResponse {...props} />}
          </Stack.Screen> 

          <Stack.Screen name='PercentPage' options={{ title: 'Вероятность' }}>
            {props => <PercentPage {...props} />}
          </Stack.Screen>

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;