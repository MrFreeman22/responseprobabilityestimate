import axios from 'axios';
import HTMLParser  from 'fast-html-parser';

export default class Api {

  getCurrentMoonPhase = async () => {
    const data = await axios.get('https://ru.astro-seek.com/faza-luny-kalendar-lunnyh-faz-onlayn');
    return HTMLParser.parse(data.data).querySelector('.astro_zn');
  }

  getGroupTimetable = async number => {
    return await axios.get(`https://journal.bsuir.by/api/v1/studentGroup/schedule?studentGroup=${number}`)
  }
}