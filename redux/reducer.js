import { 
  SET_CURRENT_MOON_PHASE, 
  SET_TIMETABLE,
  SET_CURRENT_WEEK,
  SET_WEEK_START,
  SET_WEEK_END,
  SET_GROUP_NUMBER,
  SET_IS_ACTIVE_BTN } from './actions-types';

import { 
  setCurrentMoonPhase, 
  setTimetable,
  setCurrentWeek,
  setWeekStart,
  setWeekEnd,
  setIsActiveBtn } from './actions';
import Api from './../api/api';

const api = new Api();

const inicialState = {
  currentMoonPhase: '',
  groupNumber: '',
  currentWeek: '',
  weekStart: '',
  weekEnd: '',
  timetable: [],
  isActiveBtn: true
};


export const getCurrentMoonPhase = () => async dispatch => {
  try {
    const currentMoonPhase = await api.getCurrentMoonPhase();
    // const phase = currentMoonPhase.childNodes[3].childNodes[3].childNodes[3].childNodes[0].childNodes[0].rawText;
    const phase = currentMoonPhase.childNodes[3].childNodes[3].childNodes[3].childNodes[0].rawText;
    dispatch(setCurrentMoonPhase(phase));
  } catch (error) {
    if(error.message === 'Network Error'){
      alert('Отсутствует подключение к интернету, невозможно загрузить фазу луны :(');
    }
    alert(error);
  }
}



export const getTimetable = number => async dispatch => {
  try {
    const timetable = await api.getGroupTimetable(number);
    
    const arr = timetable.data.schedules.map((day, id) => {
      const lessons = day.schedule.map((item, id) => {
        return {
          id,
          lesson: item.subject,
          auditory: item.auditory[0],
          teacher: item.employee[0].fio,
          lessonTime: item.lessonTime,
          lessonType: item.lessonType,
          weeks: item.weekNumber
        }
      });

      return {
        id,
        day: day.weekDay,
        lessons
      }
    })
    dispatch(setIsActiveBtn(false))
    dispatch(setTimetable(arr));
    dispatch(setCurrentWeek(timetable.data.currentWeekNumber));
    dispatch(setWeekStart(timetable.data.dateStart));
    dispatch(setWeekEnd(timetable.data.dateEnd));      
  } catch (error) {
    dispatch(setIsActiveBtn(true));
    dispatch(setTimetable([]));
    if(error.message === 'Network Error'){
      alert('Отсутствует подключение к интернету, невозможно загрузить расписание :(');
    }
  }
}

const reducer = (state = inicialState, action) => {
  switch(action.type){
    case SET_CURRENT_MOON_PHASE: 
      return {
        ...state,
        currentMoonPhase: action.phase
      }

    case SET_TIMETABLE: 
      return {
        ...state,
        timetable: action.timetable
      }

    case SET_CURRENT_WEEK: 
      return {
        ...state,
        currentWeek: action.currentWeek
      }

    case SET_WEEK_START: 
      return {
        ...state,
        weekStart: action.weekStart
      }

    case SET_WEEK_END: 
      return {
        ...state,
        weekEnd: action.weekEnd
      }

    case SET_GROUP_NUMBER: 
      return {
        ...state,
        groupNumber: action.groupNumber
      }

    case SET_IS_ACTIVE_BTN: 
      return {
        ...state,
        isActiveBtn: action.value
      }
      

    default: return state;
  }
}

export default reducer;