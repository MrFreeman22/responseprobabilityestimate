import { 
  SET_CURRENT_MOON_PHASE, 
  SET_TIMETABLE,
  SET_CURRENT_WEEK,
  SET_WEEK_START,
  SET_WEEK_END,
  SET_GROUP_NUMBER,
  SET_IS_ACTIVE_BTN } from './actions-types';

export const setCurrentMoonPhase = phase => {
  return {
    type: SET_CURRENT_MOON_PHASE,
    phase
  }
}

export const setTimetable = timetable => {
  return {
    type: SET_TIMETABLE,
    timetable
  }
}

export const setCurrentWeek = currentWeek => {
  return {
    type: SET_CURRENT_WEEK,
    currentWeek
  }
}

export const setWeekStart = weekStart => {
  return {
    type: SET_WEEK_START,
    weekStart
  }
}

export const setWeekEnd = weekEnd => {
  return {
    type: SET_WEEK_END,
    weekEnd
  }
}

export const setGroupNumber = groupNumber => {
  return {
    type: SET_GROUP_NUMBER,
    groupNumber
  }
}

export const setIsActiveBtn = value => {
  return {
    type: SET_IS_ACTIVE_BTN,
    value
  }
}

